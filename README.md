# SAGE2 ContextuWall Client #

This SAGE2 application is used in conjunction with the *ContextuWall* system. You can find details about this project [here](https://immersive-analytics.infotech.monash.edu/contextuwall).

To find out more about SAGE2 look [here](http://sage2.sagecommons.org/).

### License ###

Copyright Monash University, 2016 licensed under the [MIT License](http://www.opensource.org/licenses/mit-license.php)