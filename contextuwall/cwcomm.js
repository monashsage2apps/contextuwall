/**
 * simply forwards the call as broadcast to all clients, that
 * implement the function call (which is only our contextuwall server)
 * provided in data.func
 * @param wsio
 * @param data
 * @param config
 */
function cwCommunication(wsio, data, config) {
    console.log("CW Plugin callback..");

    module.parent.exports.broadcast(data.func,
        {app: data.app, data: data});

}

module.exports = cwCommunication;