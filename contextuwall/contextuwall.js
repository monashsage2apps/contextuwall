var contextuwall = SAGE2_App.extend({

    init : function(data) {
        this.SAGE2Init("div", data);

        
		this.element.style.background="#000000 url('uploads/apps/contextuwall/cave2-illustration.jpg') no-repeat center";
		
        this.resizeEvents = "continuous";
        this.moveEvents   = null;
        this.date = data.date;

		this.thumbnailimage;

        this.grid;

        this.annotations = {};

        this.master = clientID == 0;

        this.onloadcallback = function(){
            this.onimageload();
        }.bind(this);

        /*
            send command to our plugin, which broadcasts the cwAppStarted command
            to listening contextuwall servers, to indicate, the app is ready
            to receive images
         */
        if(this.master) {

            this.applicationRPC({id: this.id}, "cwAppStarted", false);
        } else {

            this.broadcast('requestState', {clientID : clientID});
        }
    },

    load : function(date) {
        console.log('[load] called');
    },

    onimageload : function() {
        console.log('[onimageload] start tiling process');

		//check, if image isn't broken
		// sometimes, images are not fully displayed, since the file was loaded
		// from the hard disk, but the file hasn't been written completely to the drive
		// resulting in incomplete images
		// Retrigger the loading
		console.log('image height: ' + this.image.naturalHeight + ' stateheight: ' + this.state.imageheight);
		console.log('image width: ' + this.image.naturalWidth + ' statewidth: ' + this.state.imagewidth);
		if(this.state.imageheight != this.image.naturalHeight 
			||this.state.imagewidth != this.image.naturalWidth ) {
			console.log('[onimageload] the displayed size of the imagee != to actual size.. trigger reloading');
			
			this.image.src = this.state.imageURL + '?dummy=' + Date.now();
			return;
		}
		
		
        var canvastag = document.createElement('canvas');
        canvastag.width = this.image.naturalWidth;
        canvastag.height = this.image.naturalHeight;
        this.canvas = canvastag.getContext('2d');
        this.canvas.drawImage(this.image, 0, 0);

        this.numVertTiles = Math.round(this.config.totalHeight /1000); //currently fixed divider
        this.tileDim = this.image.naturalHeight / this.numVertTiles;
        this.numHorTiles = Math.round(this.image.naturalWidth / (this.tileDim));
        //this.numVertTiles = 1;
        //this.numHorTiles = 1;
        this.tilewidth = this.image.naturalWidth / this.numHorTiles;
        this.tilecanvas = document.createElement('canvas');
        this.tilecontext = this.tilecanvas.getContext('2d');
        this.tilecontext.fillStyle = '#FFFFFF';

		console.log('tiles v/h ' + this.numVertTiles+ '/' + this.numHorTiles);
        this.createGrid(this.numVertTiles, this.numHorTiles);


		this.gridx = 0;
		this.gridy = 0;
		
		this.chainloadTiles();

    },

    createGrid : function(numVertTiles, numHorTiles) {
        if(this.grid) {
            return;
		}
	
        this.grid = document.createElement('div');
        this.grid.style.position = "absolute";
        this.grid.style.width = (this.sage2_width )+ "px";
        this.grid.style.height = (this.sage2_height ) + "px";
        //this.grid.style.width + 100 + '%';
        //this.grid.style.height + 100 + '%';
        this.element.insertBefore(this.grid, this.element.firstChild);
        for(var y = 0; y < numVertTiles; y++) {
            var row = document.createElement('div');
            row.style.height = 100 / numVertTiles + '%';
            row.style.display = 'block';
            this.grid.appendChild(row);

            for (var x = 0; x < numHorTiles; x++) {
                var cell = document.createElement('img');
                cell.style.width = 100 / numHorTiles + '%';
                cell.style.height = 100  + '%';
                cell.style.float = 'left';
                cell.style.margin = '0px';
                cell.style.padding = '0px';
                cell.style.background_color= "#aaaaaa";
                
                row.appendChild(cell);
            }
        }
    },

	chainloadTiles : function() {
		if(this.gridy >= this.numVertTiles){
			this.canvas = undefined;
			if(this.element.firstChild && this.thumbnailimage)
				this.element.removeChild(this.thumbnailimage);
	
			return;
		}
		setTimeout(function(){
			
			var curtileHorDim = this.tilewidth;
			var left = Math.round(this.image.naturalWidth - this.gridx * this.tilewidth - this.tilewidth);

			if(left < 0)
				curtileHorDim = this.image.naturalWidth - this.gridx * this.tilewidth;
			this.tilecanvas.width = curtileHorDim;
			this.tilecanvas.height = this.tileDim;
			var imgData = this.canvas.getImageData(this.gridx * curtileHorDim, this.gridy * this.tileDim, curtileHorDim, this.tileDim);
			this.tilecontext.putImageData(imgData, 0, 0);
			var dataurl = this.tilecanvas.toDataURL();
			this.setGridTile(this.gridx, this.gridy, dataurl);
			
			this.gridx++;
			if(this.gridx >= this.numHorTiles) {
				this.gridy++;
				this.gridx = 0;
			}
			
			this.chainloadTiles();
		}.bind(this), 0);
	},

    setGridTile : function(gridx, gridy, src) {
        //sanity check
        if(this.grid.children.length <= gridy) {
            console.log('[cwapp error]: setGridTile : gridy=' + gridy + " but num vertical tiles + " + this.element.children.length);
            return;
        }
        var row = this.grid.children[gridy];
        if(row.children.length <= gridx) {
            console.log('[cwapp error]: setGridTile : gridx=' + gridx + " but num horizontal tiles + " + row.children.length);
            return;
        }
        var col = row.children[gridx];
        col.src = src;
		//console.log('done. setting grid tile: ' + gridx + '/' + gridy + ' sourcelen ' + src.length) ;
    },

    resizeGrid : function() {

        var gridheight = Math.round(this.sage2_height / this.grid.children.length);
        var gridwidth = Math.round(this.sage2_width / this.grid.children[0].length);
        for(var row in this.grid.children) {

            for(var col in row) {

            }
        }
    },

    /*
        method, that is called via broadcast from non-master apps, to trigger
        a updatestate broadcast from the master

     */
    requestState : function(data) {
        if(this.master) {
			if(this.state.fileId == undefined) {
				console.log('[cwapp : master] [requestState] not broadcasting, since we do not have a state model yet');
			} else {
				console.log('[cwapp : master] [requestState] broadcasting updateState to slave apps');
				this.broadcast('updateState', {state: this.state});
			}
        }
    },

    /*
        broadcast callback for slave app synchronisation
     */
    updateState : function(data, mastersync) {

        console.log('cwapp [clientID: ' + clientID + ']: updating sync state');
        
		/*
			a call to this method will also set the state object and
			all necessary fields
			and displays a thumbnail, if the tiling process takes a long time
		*/
		if(data.state.thumbnail !== undefined)
			this.newthumbnailimage(data.state);

        if (data.state.imageURL !== undefined) {

            console.log('[updateState] loadingImage: ' + this.state.imageURL);
			this.newimage(data.state);
        }


        //this means.. check all annotations
        if(data.state.annotations !== undefined) {
            for(var owner in data.state.annotations) {
                console.log('[updateState] loadingAnnotation: ' + data.state.annotations[owner]);
		var realOwner = owner.substr(1); //we needed to prepend an 'o' to the object key, since its undefined in websocket, to transmit object keys starting with number
                this.newannotation({owner : owner, imageURL : data.state.annotations[owner]});
            }

        }


    },

    /*
        listener for broadcast from contextuwall server, when the server starts up and sage2 still has
        some CW windows open. Then this call will trigger an download of images to the server
        and sync it with the current state of the sage2 CW app windows
     */
    cwSyncServerRequest : function() {

        //only the master should answer
        if( ! this.master)
            return;

        console.log('[cwapp.cwSyncServerRequest] : answering contextuwallserver sync request');

        // this.state.imageURL and this.grid are only defined, if we got the an image
        if(! this.state.imageURL && this.grid == undefined) {
            console.log('[cwapp.cwSyncServerRequest] : something went wrong previously and we did not get the full image..send "undefined" back');
            this.applicationRPC({image : undefined, thumbnail : undefined}, "cwSyncServerReply", false);
            return;
        }

        // due to a bug?! in socket.io we cannot send numeric object keys
        //so we send 'o'+OwnerID
        var reformattedAnnotations = {};
        for(var owner in this.state.annotations) {
            reformattedAnnotations['o' + owner] = this.state.annotations[owner];
        }

        var relX = (this.sage2_x + this.sage2_x / 2) / (this.config.totalWidth);
        var relY = (this.sage2_y + this.sage2_y / 2) / (this.config.totalHeight);



        var d = {
            fileId : this.title,
			filename : this.filename,
			image : this.state.imageURL, //imageURL
            thumbnail : this.state.thumbnail,
            annotations : reformattedAnnotations, //object with annotations per owner and the url
            imgwidth : this.state.imagewidth, // the  image's width
            imgheight : this.state.imageheight,  // the  image's height
            imgx : relX,
            imgy : relY,
            scale : this.sage2_height / this.config.totalHeight,
            tiles : this.state.tiles
        };
        this.applicationRPC(d, "cwSyncServerReply", false);
    },



    /*
        Callback function, that gets called as reply to 'cwAppStarted', if the server finds the data model on the contextuwall server and on the sage2 server.
		This function is called, if the display was restarted
     */
    syncfromrestart : function(data) {
        if(this.master) {
            console.log('[syncfromrestart] [master] resyncing with previous session');
			// requesting current state object from contextuwall server.
			// the server will send the reply to that as broadcast to all apps on all displays
            this.broadcast('updateState', {state : data});
        }
    },

    newthumbnailimage : function(data) {
        console.log('cwapp: [newthumbnailimage] received new thumbnail');
		if(data.fileId == undefined){
			console.log('cwapp: [newthumbnailimage] ERROR: method call with invalid data object. no fileId given');
			return;
		}

        this.title = data.fileId;
        this.filename = data.filename;
        this.state.filename = data.filename;
        this.state.fileId = data.fileId;
        this.state.imagewidth = data.imagewidth;
        this.state.imageheight = data.imageheight;
        this.state.thumbnail = data.thumbnail;
        if(this.thumbnailimage == undefined && this.grid == undefined) {

            // remove background, in case, the image has transparency
            this.element.style.background_image = 'none;'

            this.thumbnailimage = new Image();
            this.thumbnailimage.style.position="absolute";
            this.thumbnailimage.style.width=this.sage2_width+1+"px";
            this.thumbnailimage.style.height=this.sage2_height+1+"px";
            this.element.insertBefore(this.thumbnailimage, this.element.firstChild);
            this.thumbnailimage.src = 'data:' + data.type + ';base64,' + data.thumbnail;
        }


    },

    /*
        called via broadcast from contextuwall server, to indicate new image
     */
    newimage : function(data) {

        
		console.log('cwapp: [newimage] received new image.. ');



        if (this.image == undefined) {

            this.element.style.background="#000000 none no-repeat center";
            // remove background, in case, the image has transparency
            this.element.style.background_image = 'none;';

            this.image = new Image();
            this.image.addEventListener('load', this.onloadcallback);

			if(data.imageURL == undefined)
				console.log('error');
            this.state.imageURL = data.imageURL;
            //prevent browser caching by using dummy parameter
            // the 'onload' callback will execute the tiling process
            this.image.src = data.imageURL + '?dummy=' + Date.now();

        }

    },



    /*
     called via broadcast from contextuwall server, to indicate new annotation
     */
    newannotation : function(data) {

       
		console.log('cwapp: [newannotation] received new annotation.. ');
		if(! this.state.annotations)
			this.state.annotations = {};
		this.state.annotations[data.owner] = data.imageURL;
		this.state.curAnnotationUpdate = data.owner;

		if(this.annotations[data.owner] == undefined) {
                this.annotations[data.owner] = new Image();
			this.annotations[data.owner].style.position = "absolute";
			this.annotations[data.owner].style.width=this.sage2_width+1+"px";
			this.annotations[data.owner].style.height=this.sage2_height+1+"px";
			this.element.appendChild(this.annotations[data.owner]);
        }
        //prevent browser caching by using dummy parameter
        this.annotations[data.owner].src = data.imageURL + '?dummy=' + Date.now();


    },


    draw : function(date) {

    },

    resize : function(date) {

		if(this.grid) {
			this.grid.style.width=this.sage2_width+"px";
			this.grid.style.height=this.sage2_height+"px";
		}

        if(this.thumbnailimage) {
            this.thumbnailimage.style.width=this.sage2_width+"px";
            this.thumbnailimage.style.height=this.sage2_height+"px";
        }

		if(this.annotations) {
			for(var owner in this.annotations) {
				this.annotations[owner].style.width=this.sage2_width+"px";
				this.annotations[owner].style.height=this.sage2_height+"px";
			}
		}


    },

    event : function(eventType, position, user, data, date) {

    }
});
